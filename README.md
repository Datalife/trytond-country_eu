datalife_country_eu
===================

The country_eu module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-country_eu/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-country_eu)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
