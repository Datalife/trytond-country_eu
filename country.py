# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction
from sql import Union
from sql.conditionals import Case

MEMBERS = [
    'DE',
    'AT',
    'BE',
    'BG',
    'CY',
    'HR',
    'DK',
    'SK',
    'SI',
    'ES',
    'EE',
    'FI',
    'FR',
    'GR',
    'HU',
    'IE',
    'IT',
    'LV',
    'LT',
    'LU',
    'MT',
    'NL',
    'PL',
    'PT',
    'CZ',
    'RO',
    'SE'
]


class Country(metaclass=PoolMeta):
    __name__ = 'country.country'

    eu_member = fields.Boolean('Belongs EU')

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        table_h = cls.__table_handler__(module_name)
        table = cls.__table__()

        column_exists = table_h.column_exist('eu_member')

        super().__register__(module_name)

        cursor.execute(*Union(
            table.select(
                table.id,
                where=((~table.eu_member) & table.code.in_(MEMBERS))),
            table.select(
                table.id,
                where=((table.eu_member) & ~table.code.in_(MEMBERS))),
            )
        )

        if not column_exists or cursor.fetchone():
            cursor.execute(*table.update(
                columns=[table.eu_member],
                values=[Case((table.code.in_(MEMBERS), True), else_=False)])
            )
